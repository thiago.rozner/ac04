FROM python:3

WORKDIR /app
copy . .

CMD [ "python", "/index.py" ]
